library(shiny)
library(ggplot2)
library(plotly)
library(data.table)
library(bit64)
getwd()

Train4 = fread("train_cleaned2.csv")

Test2 <- Train4 %>%
  group_by(Year, continent, country, channelGrouping) %>%
  summarise(Number_of_visits = sum(visits))

Test3 <- Train4 %>%
  group_by(Year, Month, continent) %>%
  summarise(Number_of_visits = sum(visits))

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("Google Store"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
          tabsetPanel(
            tabPanel("Channel Grouping",
              selectInput("year", "Select Year:", choices = unique(Train4$Year)),
              selected = unique(Train4$Year)[1],inline = FALSE,
              radioButtons("device_cat", "Select Device Category:", choices = unique(Train4$deviceCategory),
                        selected = unique(Train4$deviceCategory[1], inline = FALSE)),
              selectInput("browse", "Select Browser:", choices = unique(Train4$browser),
                        selected = unique(Train4$browser[1], inline = FALSE))
            ),
            tabPanel("Continents",
              selectInput("year2", "Select Year:", choices = unique(Test2$Year)),
              radioButtons("Continent", "Select Continent:", choices = unique(Test2$continent)),
              selectInput("Country", "Select Country:", choices = unique(Test2$country)
            ),
            tabPanel("Monthly Data",
                      selectInput("year3", "Select Year:", choices = unique(Test3$Year)),
                      selectInput("month", "Select Month:", choices = unique(Test3$Month))
                      )
          
        ),
        
        mainPanel(
            tabsetPanel( tabPanel("Number of visits through Browser from different Device Category", plotlyOutput("bar"),
                                  dataTableOutput("table"),
                                  verbatimTextOutput("summary")),
                         tabPanel("Number of visits per country ", plotlyOutput("bar2"),
                                  dataTableOutput("table2"),
                                  verbatimTextOutput("summary2")),
                         tabPanel("Number of Visits per month", plotlyOutput("Hist"),
                                  dataTableOutput("table3"),
                                  verbatimTextOutput("summary3")),
                         tabPanel("Read Me",
                                  includeMarkdown("ReadMe.Rmd"))
                         
            )
        )
    )
)


# Define server logic required to draw a histogram
server <- function(input, output, session) {

## Filtering and updating the browser with respective device Category.

    filtered_data <- reactive({filter(Train4, Year == input$year, deviceCategory == input$device_cat)})
    
    observe({
      updateSelectInput(session, "browse", choices = sort(unique(filtered_data()$browser)))
      })
    
    filtered_data2 <- reactive({filter(filtered_data(), browser == input$browse)})
    
## Bar plot between Channel Grouping and Number of visits through Browser from different Device Category 

    output$bar <- renderPlotly(
        {
          Channel_Grouping_and_Number_of_visits_through_Browser_from_different_Device_Category <- ggplot(filtered_data2(), aes(channelGrouping, fill = channelGrouping)) + geom_bar() + xlab("Types of Channel_Grouping") + ylab("Count") +
              ggtitle("Bar plot of Channel Grouping with various platforms") + 
              theme_classic()
            ggplotly(Channel_Grouping_and_Number_of_visits_through_Browser_from_different_Device_Category )
        }
    )
    
## Gives the data table with repective selections 

    output$table <- renderDataTable({
      select(filtered_data2(), c("Year", "channelGrouping", "deviceCategory", "browser"))
    })

## Gives the summary table with repective selections  
    
    output[['summary']] <- renderPrint({
      dataset <- filtered_data()
      summary(dataset)
    })
    
## Filtering and updating the conutries with respective continent.

    filtered_data3 <- reactive({filter(Test2, Year == input$year2, continent == input$Continent)})
    observe({
      updateSelectInput(session, "Country", choices = sort(unique(filtered_data3()$country)))
    })

    filtered_data4 <- reactive({filter(filtered_data3(), country == input$Country)})

## Bar plot between Continets and Number of visits per country

    output$bar2 <- renderPlotly(
      {
        Continets_and_Number_of_visits_per_country <- ggplot(filtered_data4(), aes(x = channelGrouping, y = Number_of_visits, fill = channelGrouping)) + geom_bar(stat = "Identity") + xlab("Continents") + ylab("Number of Visits") +
          ggtitle("Bar plot of Number of visits per Country") + theme(axis.text.x = element_text(angle=90, hjust=1))
        ggplotly(Continets_and_Number_of_visits_per_country)
      }
    )

## Gives the data table with repective selections

    output$table2 <- renderDataTable({
      head(filtered_data4())
    })

## Gives the summary table with repective selections

    output[['summary2']] <- renderPrint({
      dataset2 <- filtered_data3()
      summary(dataset2)
    })
  
## Filtering and updating the month with respective year
#
    filtered_data5 <- reactive({filter(Test3, Year == input$year3)})

    observe({
      updateSelectInput(session, "month", choices = sort(unique(filtered_data5()$Month)))
    })
#
    filtered_data6 <- reactive({filter(filtered_data5(), Month == input$month)})

## Histogram between Continets and Number of visits per month

    output$Hist <- renderPlotly(
      {
        Continets_and_Number_of_visits_per_month  <- ggplot(filtered_data6(), aes(Number_of_visits, fill = continent)) + geom_histogram(binwidth = 1000)+ xlab("Range") + ylab("Count") +
          ggtitle("Histogram of Number of visits per Month per Country") + theme_classic()
        ggplotly(Continets_and_Number_of_visits_per_month)
      }
    )

## Gives the data table with repective selections

    output$table3 <- renderDataTable({
      head(filtered_data6())
    })

## Gives the summary table with repective selections

    output[['summary3']] <- renderPrint({
      dataset2 <- filtered_data6()
      summary(dataset2)
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
